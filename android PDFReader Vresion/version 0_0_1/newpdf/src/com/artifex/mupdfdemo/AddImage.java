package com.artifex.mupdfdemo;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;

public class AddImage {
	String imgPath;
	String savePath;
	String currentPath;
	
	public AddImage(String imgP,String saveP,String currentP )
	{
		imgPath=imgP;
		savePath=saveP;
		currentPath=currentP;
		AddImageWatermark();
	}
	
    public void AddImageWatermark() {
        try {
        	
        	
        	
            PdfReader pdfReader = new PdfReader(currentPath);
 
            PdfStamper pdfStamper = new PdfStamper(pdfReader,new FileOutputStream(savePath+".pdf"));
 
            Image image = Image.getInstance(imgPath);
            image.scaleToFit(200, 200);
            for(int i=1; i<= pdfReader.getNumberOfPages(); i++){
 
                //put content under
                PdfContentByte content = pdfStamper.getUnderContent(i);
                image.setAbsolutePosition(100f, 170f);
                
                content.addImage(image);
 
                //put content over
//                content = pdfStamper.getOverContent(i);
//                image.setAbsolutePosition(300f, 150f);
//                content.addImage(image);
 
                //Text over the existing page
//                BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA,
//                        BaseFont.WINANSI, BaseFont.EMBEDDED);
//                content.beginText();
//                content.setFontAndSize(bf, 18);
//                content.showTextAligned(PdfContentByte.ALIGN_LEFT,"Page No: " + i,430,15,0);
//                content.endText();
 
            }
 
            pdfStamper.close();
            pdfReader.close();
 
        } catch (IOException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }
		 
}
