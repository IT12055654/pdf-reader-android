package com.artifex.mupdfdemo;



import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import com.example.newpdf.R;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;

public class ImageViewer extends Activity {
	//variable for selection intent
		private final int PICKER = 1;
		//variable to store the currently selected image
		private int currentPic = 0;
		//gallery object
		private Gallery picGallery;
		//image view for larger display
		private ImageView picView;
		//adapter for gallery view
		private PicAdapter imgAdapt;
		
		

		
		@Override
		protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.activity_image_viewer);
			
		// get the large image view
		picView = (ImageView) findViewById(R.id.imageView1);

		// get the gallery view
		picGallery = (Gallery) findViewById(R.id.gallery1);

		String stringExtra = getIntent().getStringExtra("PATH");
		if (stringExtra != null && !stringExtra.equalsIgnoreCase("")) {
			Log.v("PDF PATH", stringExtra);
			imgAdapt = new PicAdapter(this, getBitmapList(stringExtra));
			picGallery.setAdapter(imgAdapt);
		} else {
			// create a new adapter
			imgAdapt = new PicAdapter(this, new ArrayList<Bitmap>());
			picGallery.setAdapter(imgAdapt);
		}
		// set the gallery adapter


		// set the click listener for each item in the thumbnail gallery
		picGallery.setOnItemClickListener(new OnItemClickListener() {
			// handle clicks
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				// set the larger image view to display the chosen bitmap
				// calling method of adapter class
				picView.setImageBitmap(imgAdapt.getPic(position));
			}
		});	
			
		}
		
		
		
		private ArrayList<Bitmap> getBitmapList(String stringExtra) {
			ArrayList<Bitmap> arrayList=new ArrayList<Bitmap>();
			File file=new File(stringExtra);//new File(stringExtra.substring(0, stringExtra.lastIndexOf("/")+1));
			Log.v("DIR PATH", file.getPath());
			if (file.isDirectory()) {
				File[] listFiles = file.listFiles();
				for (File file2 : listFiles) {
					if (file2.getPath().contains(".jpg")) {
						Log.v("JPG PATH", file2.getPath());
						Bitmap bitmap=decodeFile(file2.getPath(), 100, 100);
						arrayList.add(bitmap);
					}
				}
			}
			return arrayList;
		}

		public static Bitmap decodeFile(String filePath, int WIDTH, int HIGHT) {
			try {

				File f = new File(filePath);

				BitmapFactory.Options o = new BitmapFactory.Options();
				o.inJustDecodeBounds = true;
				BitmapFactory.decodeStream(new FileInputStream(f), null, o);

				final int REQUIRED_WIDTH = WIDTH;
				final int REQUIRED_HIGHT = HIGHT;
				int scale = 1;
				while (o.outWidth / scale / 2 >= REQUIRED_WIDTH
						&& o.outHeight / scale / 2 >= REQUIRED_HIGHT)
					scale *= 2;

				BitmapFactory.Options o2 = new BitmapFactory.Options();
				o2.inSampleSize = scale;
				return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			return null;
		}

		public class PicAdapter extends BaseAdapter {
			//use the default gallery background image
			int defaultItemBackground;
			         
			//gallery context
			private Context galleryContext;
			 
			//array to store bitmaps to display
			//private Bitmap[] imageBitmaps;
			private ArrayList<Bitmap> imageBitmaps;
			 
			//placeholder bitmap for empty spaces in gallery
			Bitmap placeholder; 
			
			public PicAdapter(Context c,ArrayList<Bitmap> imageBitmaps) {
				 
			    //instantiate context
			    galleryContext = c;
			 
			    //create bitmap array
			    this.imageBitmaps  = imageBitmaps;
			             
			
			    
			    
			}
			
			//return number of data items i.e. bitmap images
			public int getCount() {
			    return imageBitmaps.size();
			}
			
			//return item at specified position
			public Object getItem(int position) {
			    return position;
			}
			
			//return item ID at specified position
			public long getItemId(int position) {
			    return position;
			}
			
			//get view specifies layout and display options for each thumbnail in the gallery
			public View getView(int position, View convertView, ViewGroup parent) {
			 
			    //create the view
			    ImageView imageView = new ImageView(galleryContext);
			    //specify the bitmap at this position in the array
			    imageView.setImageBitmap(imageBitmaps.get(position));
			    //set layout options
			    imageView.setLayoutParams(new Gallery.LayoutParams(300, 200));
			    //scale type within view area
			    imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
			    //set default gallery item background
			    imageView.setBackgroundResource(defaultItemBackground);
			    //return the view
			    return imageView;
			}
			
			//helper method to add a bitmap to the gallery when the user chooses one
			public void addPic(Bitmap newPic)
			{
			    //set at currently selected index
			    //imageBitmaps[currentPic] = newPic;
			}
			
			
			//return bitmap at specified position for larger display
			public Bitmap getPic(int posn)
			{
			    //return bitmap at posn index
			    return imageBitmaps.get(posn);
			}
		}
}
