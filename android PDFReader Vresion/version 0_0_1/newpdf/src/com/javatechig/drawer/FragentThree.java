package com.javatechig.drawer;



import java.io.File;
import java.sql.Date;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.artifex.mupdfdemo.MuPDFActivity;
import com.example.newpdf.R;











import android.app.ListFragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;


import android.view.View;

import android.widget.ListView;
import android.widget.Toast;

public class FragentThree  extends ListFragment {
	private File currentDir;
    private FileArrayAdapter adapter;
	
    
    
    @Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		 currentDir = new File("/sdcard/");
	        fill(currentDir); 
	}

	


	 private void fill(File f)
	    {
	    	File[]dirs = f.listFiles(); 
	//		 this.setTitle("Current Dir: "+f.getName());
			 List<Item>dir = new ArrayList<Item>();
			 List<Item>fls = new ArrayList<Item>();
			 try{
				 for(File ff: dirs)
				 { 
					Date lastModDate = new Date(ff.lastModified()); 
					DateFormat formater = DateFormat.getDateTimeInstance();
					String date_modify = formater.format(lastModDate);
					if(ff.isDirectory()){
						
						
						File[] fbuf = ff.listFiles(); 
						int buf = 0;
						if(fbuf != null){ 
							buf = fbuf.length;
						} 
						else buf = 0; 
						String num_item = String.valueOf(buf);
						if(buf == 0) num_item = num_item + " item";
						else num_item = num_item + " items";
						
						//String formated = lastModDate.toString();
						dir.add(new Item(ff.getName(),num_item,date_modify,ff.getAbsolutePath(),"open1271")); 
					}
					else
					{
						if(ff.getName().toLowerCase().endsWith(".pdf"))
						fls.add(new Item(ff.getName(),ff.length() + " Byte", date_modify, ff.getAbsolutePath(),"adobe211"));
					}
				 }
			 }catch(Exception e)
			 {    
				 
			 }
			 Collections.sort(dir);
			 Collections.sort(fls);
			 dir.addAll(fls);
			 if(!f.getName().equalsIgnoreCase("sdcard"))
				 dir.add(0,new Item("..","Parent Directory","",f.getParent(),"directory_up"));
			 adapter = new FileArrayAdapter(getActivity(),R.layout.fragment_layout3,dir);
			 this.setListAdapter(adapter); 
	    }

	public FragentThree() {
		// TODO Auto-generated constructor stub
	}
	
		
	
	
/*	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view=inflater.inflate(R.layout.fragment_layout3,container, false);
		
		
		
		return view;
	}
	*/
	  @Override
	public void onListItemClick(ListView l, View v, int position, long id) {
			// TODO Auto-generated method stub
			super.onListItemClick(l, v, position, id);
			Item o = adapter.getItem(position);
			if(o.getImage().equalsIgnoreCase("open1271")||o.getImage().equalsIgnoreCase("directory_up")){
					currentDir = new File(o.getPath());
					fill(currentDir);
			}
			else
			{
				onFileClick(o);
			}
		}
	    private void onFileClick(Item o)
	    {
	    	Toast.makeText(getActivity(), "Folder Clicked: "+ currentDir, Toast.LENGTH_SHORT).show();
	    	
	    	
	   /* 	OpenFileActivity fragment = new OpenFileActivity (o);
			
			
			FragmentManager fragmentManager = getFragmentManager();

			// Creating a fragment transaction
			FragmentTransaction ft = fragmentManager.beginTransaction();

			// Adding a fragment to the fragment transaction
			ft.replace(R.id.content_frame, fragment);
			// Committing the transaction
			ft.addToBackStack(o.getName());
			ft.commit();
		
	    	
	    	
	    */
	    	Uri uri = Uri.parse(o.getPath());
		    Intent intent = new Intent(getActivity(), MuPDFActivity.class);
		    intent.setAction(Intent.ACTION_VIEW);
		    intent.setData(uri);
		   startActivity(intent);
	    
	    }
	    


}
